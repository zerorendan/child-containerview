//
//  App.swift
//  ViewProject
//
//  Created by Juan Calvo on 8/15/18.
//  Copyright © 2018 Juan Calvo. All rights reserved.
//

import UIKit
import SwiftyJSON

class App: NSObject {
    var id: String
    var name: String
    var artworkUrl: String
    
    init(appDetails:JSON) {
        name = appDetails[Constants.ApiKeys.App.name].string ?? Constants.defaultTextApi
        artworkUrl = appDetails[Constants.ApiKeys.App.artworkUrl].string ?? Constants.defaultTextApi
        id = appDetails[Constants.ApiKeys.App.id].string ?? Constants.defaultTextApi
    }
}
