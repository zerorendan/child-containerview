//
//  LowerTableViewCell.swift
//  ViewProject
//
//  Created by Juan Calvo on 8/15/18.
//  Copyright © 2018 Juan Calvo. All rights reserved.
//

import UIKit

class LowerTableViewCell: UITableViewCell {
    @IBOutlet var titleLabel: UILabel!
    @IBOutlet var nameLabel: UILabel!
    
    func configure(title:String, name:String) {
        titleLabel.text = title
        nameLabel.text = name
    }
}
