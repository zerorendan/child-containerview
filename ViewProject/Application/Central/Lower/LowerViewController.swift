//
//  LowerViewController.swift
//  ViewProject
//
//  Created by Juan Calvo on 8/15/18.
//  Copyright © 2018 Juan Calvo. All rights reserved.
//

import UIKit

protocol LowerViewControllerDelegate {
    func didSelect(app:App)
}

class LowerViewController: UIViewController {
    @IBOutlet var appTable: UITableView!
    
    private let cellId = "AppCell"
    let connection = ConnectionManager.shared
    var data:[App] = []
    var delegate: LowerViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registerAutoreload()
        showLoading()
        connection.getData { apps in
            self.data.append(contentsOf: apps)
            self.hideLoading()
            NotificationCenter.default.post(
                name: NSNotification.Name(Constants.reloadTableNotificationName),
                object: nil)
        }
    }
    
    @objc private func reloadData() {
        appTable.reloadData()
    }
    
    private func registerAutoreload() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(LowerViewController.reloadData),
                                               name: NSNotification.Name(Constants.reloadTableNotificationName),
                                               object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self,
                                                  name: NSNotification.Name(Constants.reloadTableNotificationName),
                                                  object: nil)
    }
}

extension LowerViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! LowerTableViewCell
        cell.configure(title: data[indexPath.row].id, name: data[indexPath.row].name)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard data.count > 0 else {return}
        delegate?.didSelect(app: data[indexPath.row])
    }
}
