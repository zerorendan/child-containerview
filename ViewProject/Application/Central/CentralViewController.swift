//
//  CentralViewController.swift
//  ViewProject
//
//  Created by Juan Calvo on 8/15/18.
//  Copyright © 2018 Juan Calvo. All rights reserved.
//

import UIKit

class CentralViewController: UIViewController {
    
    @IBOutlet var upperViewContainer: UIView!
    fileprivate var lowerController: LowerViewController?
    fileprivate var upperController: UpperViewController? 
    
    override func viewDidLoad() {
        super.viewDidLoad()
        guard childViewControllers.first is LowerViewController else { return }
        lowerController = childViewControllers.first as? LowerViewController
        guard lowerController != nil else { return }
        lowerController?.delegate = self
        upperController = UIViewController.generateController(
            controller: Constants.ViewController.upper,
            fromStoryboard: Constants.Storyboards.upper) as? UpperViewController
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

extension CentralViewController : LowerViewControllerDelegate {
    func didSelect(app: App) {
        upperController?.appIconUrl = app.artworkUrl
        if childViewControllers.count > 1 {
            removeChildController(upperController!, toView: upperViewContainer)
        }
        addChildController(upperController!, toView: upperViewContainer)
    }
}
