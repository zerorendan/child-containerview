//
//  UpperViewController.swift
//  ViewProject
//
//  Created by Juan Calvo on 8/15/18.
//  Copyright © 2018 Juan Calvo. All rights reserved.
//

import UIKit

class UpperViewController: UIViewController {
    @IBOutlet var appIcon: UIImageView!
    var appIconUrl: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        guard appIconUrl != nil else { return }
        appIcon.setWebImage(url: appIconUrl!)
    }
    
}
