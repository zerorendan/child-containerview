//
//  ConnectionManager.swift
//  ViewProject
//
//  Created by Juan Calvo on 8/15/18.
//  Copyright © 2018 Juan Calvo. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class ConnectionManager {
    // MARK: - Propiertes
    static let shared = ConnectionManager()
    let sessionManager: SessionManager
    let utilityQueue: DispatchQueue
    
    // MARK: - Init
    private init() {
        let extendedTimeoutConfiguration = URLSessionConfiguration.default
        extendedTimeoutConfiguration.timeoutIntervalForRequest = Constants.connectionTimeout
        extendedTimeoutConfiguration.timeoutIntervalForResource = Constants.connectionTimeout
        sessionManager = Alamofire.SessionManager(configuration: extendedTimeoutConfiguration)
        utilityQueue = DispatchQueue.global(qos: .utility)
    }
    
    func getData(onSucces: @escaping (([App]) -> Void)) {
        var apps:[App] = []
        sessionManager.request(URL(string: Constants.apiUrl)!).responseJSON { response in
            switch response.result {
            case .success(let data):
                let json = JSON(data)[Constants.ApiKeys.Other.feed][Constants.ApiKeys.Other.results]
                guard json != .null else { return }
                for values in json.array! {
                    apps.append(App(appDetails: values))
                }
                onSucces(apps)
            case .failure(let error):
                print(error.localizedDescription)
            }
        }
    }
}
