//
//  Constants.swift
//  ViewProject
//
//  Created by Juan Calvo on 8/15/18.
//  Copyright © 2018 Juan Calvo. All rights reserved.
//

import Foundation
struct Constants {
    static let connectionTimeout:TimeInterval = 45
    static let apiUrl = "https://rss.itunes.apple.com/api/v1/us/ios-apps/top-paid/games/25/explicit.json"
    static let defaultTextApi = "unknown"
    static let reloadTableNotificationName = "RELOADTABLENOTIFICATION"
    
    struct ApiKeys {
        struct Other {
            static let feed = "feed"
            static let results = "results"
        }
        
        struct App {
            static let id = "id"
            static let artworkUrl = "artworkUrl100"
            static let name = "name"
        }
    }
    
    struct Storyboards {
        static let upper = "Upper"
    }
    
    struct ViewController {
        static let upper = "UpperViewController"
    }
}
