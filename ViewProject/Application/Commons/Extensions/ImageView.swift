//
//  ImageView.swift
//  ViewProject
//
//  Created by Juan Calvo on 8/15/18.
//  Copyright © 2018 Juan Calvo. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

extension UIImageView {
    func setWebImage(url:String) {
        self.sd_setImage(with: URL(string: url)!, placeholderImage: #imageLiteral(resourceName: "placeholder"))
    }
}
