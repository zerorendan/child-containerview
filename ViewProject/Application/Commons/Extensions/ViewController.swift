//
//  ViewController.swift
//  ViewProject
//
//  Created by Juan Calvo on 8/15/18.
//  Copyright © 2018 Juan Calvo. All rights reserved.
//

import UIKit
import MBProgressHUD

extension UIViewController {
    public static func generateController(controller controllerName: String, fromStoryboard storyboardName: String) -> UIViewController {
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: controllerName)
    }
    
    func addChildController(_ child: UIViewController, toView: UIView) {
        addChildViewController(child)
        toView.addSubview(child.view)
        child.didMove(toParentViewController: self)
    }
    
    func removeChildController (_ child: UIViewController, toView: UIView) {
        child.willMove(toParentViewController: nil)
        toView.willRemoveSubview(child.view)
        child.view.removeFromSuperview()
        child.removeFromParentViewController()
    }
    
    func showLoading() {
        MBProgressHUD.showAdded(to: self.view, animated: true)
    }
    
    func hideLoading() {
        MBProgressHUD.hide(for: self.view, animated: true)
    }
    
}
